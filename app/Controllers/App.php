<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function sharer()
    {
        $data = [];

        $fb_url = ' https://www.facebook.com/sharer/sharer.php?u=' . site_url();
        $fb_quote = get_field('facebook_message', 'options') ? '&quote=' . get_field('facebook_message', 'options')
            : '';

        $data['facebook'] = $fb_url . $fb_quote;

        return $data ? (object) $data : '';
    }
}

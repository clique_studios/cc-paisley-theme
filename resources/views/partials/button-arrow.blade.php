<svg width="6px" height="11px" viewBox="0 0 6 11" preserveAspectRatio="xMidYMid slice">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(-869.000000, -497.000000)" fill="{{ $color }}">
            <g transform="translate(683.000000, 482.000000)">
                <polygon
                        transform="translate(189.000000, 20.500000) rotate(-270.000000) translate(-189.000000, -20.500000) "
                        points="189 18 194 23 184 23"></polygon>
            </g>
        </g>
    </g>
</svg>
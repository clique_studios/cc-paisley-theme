<footer id="footer" class="footer">
    <h2>{!! get_field('footer_heading', 'options') !!}</h2>
    @if(have_rows('social_links', 'options'))
        <ul class="social-links">
            @while(have_rows('social_links', 'options'))@php the_row() @endphp
            <li>
                @if(strtolower(get_sub_field('social_link')['title']) === "facebook")
                    @php $url = $sharer->facebook @endphp
                @else
                    @php $url = get_sub_field('social_link')['url']@endphp
                @endif
                <a href="{!! $url !!}"
                   target="{{ get_sub_field('social_link')['target']}}">
                    <span>{!! get_sub_field('social_link')['title'] !!}</span>
                    @if(strtolower(get_sub_field('social_link')['title']) === "facebook")
                        <img src="@asset('images/footer-facebook-icon.svg')" alt="facebook">
                    @elseif (strtolower(get_sub_field('social_link')['title']) === "email")
                        <img src="@asset('images/footer-mail-icon.svg')" alt="facebook">
                    @endif
                </a>
            </li>
            @endwhile
        </ul>
        @if (has_nav_menu('footer_navigation'))
            {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'footer-nav', 'container' =>
            'false']) !!}
        @endif
        <div class="logo">
            <img src="{{ get_field('footer_logo', 'options')['url'] }}"
                 alt="{{ get_field('footer_logo', 'options')['alt'] }}">
        </div>
        <p class="disclaimer">{!! get_field('footer_disclaimer', 'options') !!}</p>
    @endif
</footer>

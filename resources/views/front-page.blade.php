@extends('layouts.app')

@section('content')
    <section id="banner" class="patients-hero is-animating" style="">
        <div class="banner-container">
            <div class="patients-hero__word-cloud">
                <h1>
        <span>
          <span>Which</span> <strong>Lupus</strong> <span>Did You</span>
        </span>
                    <span>
          <span>Wake Up With</span> <strong>Today</strong>
        </span>
                    <span>?</span>
                </h1>
                @if(have_rows('banner_word_cloud'))
                    <ul>
                        @while(have_rows('banner_word_cloud'))@php the_row() @endphp
                        <li><span>{!! get_sub_field('word') !!}</span></li>
                        @endwhile
                    </ul>
                @endif
            </div>
            <div class="patients-hero__image">
                @if(get_field('banner_image'))
                    <img src="{!! get_field('banner_image')['url'] !!}" alt="{!! get_field('banner_image')['alt']
                    !!}">
                @endif
            </div>
            @if(get_field('banner_overlay'))
                @php $overlay = get_field('banner_overlay') @endphp
                <div class="patients-hero__cta">
                    <div>
                        <p>{!! $overlay['short_description'] !!}</p>
                        <a class="btn--hero-cta pre-qualify white"
                           href="#" target="{{ $overlay['cta']['target'] }}">
                            <span>{!! $overlay['cta']['title'] !!}</span>
                            @include('partials.button-arrow', ['color' => '#B41D8B'])
                        </a>
                        <div class="tooltip">
                            <h6>{!! $overlay['disclaimer_text'] !!}</h6>
                            <a href="#" role="button" data-tooltip="true">?</a>
                            <p>{!! $overlay['tooltip_text'] !!}</p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="short-paragraph">
            <p>{!! get_field('banner_short_paragraph') !!}</p>
            @include('partials.textures.single-butterfly')
        </div>
    </section>
    <section id="about-the-study">
        <h2>{!! get_field('about_headline') !!}</h2>
        <div class="text-cnt">{!! get_field('about_text') !!}</div>
        <a href="{{ get_field('about_cta')['url'] }}" class="btn--hero-cta pre-qualify">
            <span>{!! get_field('about_cta')['title'] !!}</span>
            @include('partials.button-arrow', ['color' => '#FFFFFF'])
        </a>
        <div class="tooltip">
            <h6>{!! get_field('about_disclaimer_text') !!}</h6>
            <a href="#" role="button" data-tooltip="true">?</a>
            <p>{!! get_field('about_tooltip_text') !!}</p>
        </div>
    </section>
    <section id="pre-qualify">
        <div class="short-paragraph">
            <p>{!! get_field('pq_text') !!}</p>
            @include('partials.textures.single-butterfly')
        </div>
    </section>
    <section id="to-participate">
        <h2>To Participate</h2>
        @if(have_rows('tp_text_groups'))
            @while(have_rows('tp_text_groups'))@php the_row() @endphp
            <h3>{!! get_sub_field('subheadline') !!}</h3>
            <div class="text-cnt">{!! get_sub_field('text') !!}</div>
            @endwhile
        @endif
        <a href="{{ get_field('tp_cta')['url'] }}" class="btn--hero-cta pre-qualify">
            <span>{!! get_field('tp_cta')['title'] !!}</span>
            @include('partials.button-arrow', ['color' => '#FFFFFF'])
        </a>
        <div class="tooltip">
            <h6>{!! get_field('tp_disclaimer_text') !!}</h6>
            <a href="#" role="button" data-tooltip="true">?</a>
            <p>{!! get_field('tp_tooltip_text') !!}</p>
        </div>
    </section>
    <section id="talking-to-doctor">
        <div class="row">
            <div class="col-xs-6">
                <div class="image-container">
                    <img src="{{ get_field('talking_image')['url'] }}" alt="{{ get_field('talking_image')['alt'] }}">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="text-cnt">
                    <h2>{{ get_field('talking_headline') }}</h2>
                    <div class="text-cnt">
                        <p>{!! get_field('talking_short_paragraph') !!}</p>
                    </div>
                    @if(get_field('talking_pdf_file'))
                        <a href="{{ get_field('talking_pdf_file')['url'] }}" class="btn--hero-cta white"
                           target="_blank" download>
                            <span>{!! get_field('talking_cta')['title'] !!}</span>
                            @include('partials.button-arrow', ['color' => '#B41D8B'])
                        </a>
                    @endif
                </div>
                <div class="background"></div>
            </div>
        </div>
    </section>
    <section id="faq">
        <h2>{!! get_field('faq_headline') !!}</h2>
        <div class="buttons-container">
            <button class="expand-btn">{!! get_field('faq_expand_cta')['title'] !!}</button>
            <button class="collapse-btn">{!! get_field('faq_collapse_cta')['title'] !!}</button>
        </div>
        <div class="faq-container">
            @if(have_rows('faq_groups'))
                @while(have_rows('faq_groups'))@php the_row() @endphp
                <div class="faq-group">
                    <h3>{!! get_sub_field('subheadline') !!}</h3>
                    @if(have_rows('faq_items'))
                        @php $faq_index = 1; @endphp
                        @while(have_rows('faq_items'))@php the_row() @endphp
                        <div class="faq-item">
                            <a href="" class="question" aria-expanded="false" aria-controls="answer-{{ $faq_index
                                }}">
                                <img src="@asset('images/faq-arrow.svg')" alt="faq arrow">
                                <p>{!! get_sub_field('question') !!}</p>
                            </a>
                            <div id="answer-{{ $faq_index }}" class="answer">{!! get_sub_field('answer') !!}</div>
                        </div>
                        @php $faq_index++ @endphp
                        @endwhile
                    @endif
                </div>
                @endwhile
            @endif
        </div>
    </section>
    <button id="back-to-top">
        <div class="btt--inner">
            <span>Back to top of page</span>
            <img src="@asset('images/back-to-top-arrow.svg')" alt="Back to Top of Page">
        </div>
    </button>
@endsection
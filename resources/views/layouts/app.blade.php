<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @php body_class() @endphp>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K64W2KB"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@php do_action('get_header') @endphp
@include('partials.header')
<main class="main">
    @yield('content')
</main>
@if(get_field('pq_modal_iframe_src'))
    <div id="pre-qualify-modal">
        <div class="modal--screener">
            <div class="modal--screener--inner">
                <div class="modal__background"></div>
                <div class="modal__content">
                    <div class="modal__content--inner">
                        <h2 class="type--section-heading">{!! get_field('pq_modal_headline') !!}</h2>
                        <div class="modal__iframe" data-src="{{ get_field('pq_modal_iframe_src') }}">
                            <div class="modal__close">
                                <a href="" data-modal="screener-exit">
                                    <svg width="32px" height="32px" viewBox="0 0 32 32">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g fill-rule="nonzero">
                                                <circle id="Oval" fill="#B41D8B" cx="16" cy="16" r="16"></circle>
                                                <polygon id="Path" fill="#FFFFFF"
                                                         points="8 12 12 8 24 20 20 24"></polygon>
                                                <polygon id="Path" fill="#FFFFFF"
                                                         points="24 12 20 8 8 20 12 24"></polygon>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal--exit">
            <div class="modal__background"></div>
            <div class="text-cnt">
                <h2 class="type--section-heading" tabindex="-1"
                    aria-label="Are you sure you want to close?">{!! get_field('pq_modal_exit_headline') !!}</h2>

                <p aria-hidden="true">{!! get_field('pq_modal_exit_text') !!}</p>

                <div class="modal__buttons">
                    <a href="https://www.paisleystudy.com/" aria-label="No" class="btn--white btn--large btn--hairline
                modal-no"
                       data-modal="stay-on-screener">
                        <span tabindex="1">No</span>
                    </a>
                    <a href="https://www.paisleystudy.com/en-us#" role="button" aria-label="Yes, close"
                       class="btn--pink btn--large modal-leave" data-modal="leave-screener" title="modal-close">
                        <span tabindex="1">Yes, close</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif
@php do_action('get_footer') @endphp
@include('partials.footer')
@php wp_footer() @endphp
</body>
</html>

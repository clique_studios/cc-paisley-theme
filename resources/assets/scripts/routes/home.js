export default {
    init() {
        // JavaScript to be fired on the home page
    },
    finalize() {
        // JavaScript to be fired on the home page, after the init JS
        $(document).ready(function () {
            setTimeout(function () {
                $("#banner").addClass("ready-for-hero-animation");
            }, 250);
        });

        //Tooltip Hover
        var toolTip;
        $(".tooltip a").on("mouseenter", function () {
            toolTip = $(this).parent();
            console.log(toolTip);
            toolTip.addClass("has-open-tooltip");

            toolTip.on("mouseleave", function () {
                $(this).removeClass("has-open-tooltip");
            });
        });

        $(".tooltip a").on("click", function (e) {
            e.preventDefault();
        });

        function centerImage(imageContainer) {
            //set size
            var th = imageContainer.height(),//box height
                tw = imageContainer.width(),//box width
                im,//image
                ih,//initial image height
                iw;//initial image width
            if (imageContainer.children('img').length > 0) {
                im = imageContainer.children('img');
                ih = im.height();
                iw = im.width();
            } else if (imageContainer.children('picture').length > 0) {
                im = imageContainer.children('img');
                ih = im.height();
                iw = im.width();
            }

            if ((th / tw) > (ih / iw)) {
                im.addClass('wh').removeClass('ww');//set height 100%
            } else {
                im.addClass('ww').removeClass('wh');//set width 100%
            }

            // set offset
            var nh = im.height(),//new image height
                nw = im.width(),//new image width
                hd = (nh - th) / 2,//half dif img/box height
                wd = (nw - tw) / 2;//half dif img/box width
            if (hd < 1) {
                hd = 0;
            }
            if (wd < 1) {
                wd = 0;
            }

            im.css({marginLeft: '-' + wd + 'px', marginTop: '0px'});//offset left
        }

        // Center Image
        if ($(".image-container").length > 0) {
            $(".image-container").each(function () {
                var imageContainer = $(this);
                $(window).ready(function () {
                    centerImage(imageContainer);
                });
            });

            $(window).on("resize", function () {
                if ($(".image-container").length > 0) {
                    $(".image-container").each(function () {
                        var imageContainer = $(this);
                        setTimeout(function () {
                            centerImage(imageContainer);
                        }, 10)
                    })
                }
            });
        }

        // FAQ
        $(".question").on("click", function (e) {
            e.preventDefault();
            var question = $(this);
            var faq_item = question.parent();
            var answer = question.next('.answer');
            faq_item.toggleClass("open");
            if(faq_item.hasClass("open")){
                answer.slideDown();
                question.attr("aria-expanded", true);
            } else {
                answer.slideUp();
                question.attr("aria-expanded", false);
            }
        });

        $(".expand-btn").on("click", function (e) {
            $("#faq .faq-item").addClass("open");
            $("#faq .question").attr("aria-expanded", true);
            $("#faq .answer").slideDown();
        });

        $(".collapse-btn").on("click", function (e) {
            $("#faq .faq-item").removeClass("open");
            $("#faq .question").attr("aria-expanded", false);
            $("#faq .answer").slideUp();
        });

        //Pre Qualify Modal
        var button;
        $(".pre-qualify").on("click", function (e) {
            e.preventDefault();
            button = $(this);
            $("#pre-qualify-modal").addClass("show");
            $("body").addClass("no-scroll");
            $(".modal__close a").focus();
            var iframeSrc = $("#pre-qualify-modal").find(".modal__iframe").attr("data-src");
            var iframe = "<iframe src='" + iframeSrc + "' width='100%' height='100%'></iframe>";
            $("#pre-qualify-modal .modal__iframe").append(iframe);
        });

        $(".modal__close a").on("click", function (e) {
            e.preventDefault();
            $(".modal--exit").addClass("show");
            $(".modal-no").focus();
        });

        $(".modal-leave").on("click", function (e) {
            e.preventDefault();
            $("#pre-qualify-modal, .modal--exit").removeClass("show");
            button.focus();
            $("body").removeClass("no-scroll");
            $("#pre-qualify-modal .modal__iframe").empty();
        });

        $(".modal-no").on("click", function (e) {
            $(".modal__close a").focus();
            $(".modal--exit").removeClass("show");
        })
    },
};

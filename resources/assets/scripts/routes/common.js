export default {
    init() {
        // JavaScript to be fired on all pages
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
        //Header
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 0) {
                $("header.header").addClass("headroom--not-top");
            } else {
                $("header.header").removeClass("headroom--not-top");
            }
        });

        $(".header__trigger").on("click", function () {
            if ($(window).width() < 900) {
                $("header").toggleClass("has-open-nav");
            }
        });

        function scrollToAnchor(aid) {
            var aTag = $(aid);
            $('html,body').animate({scrollTop: aTag.offset().top - 56}, 'slow');
        }

        $("header a").on("click", function (e) {
            var href = $(this).attr("href");
            if (href.charAt(0) === "#") {
                scrollToAnchor(href);
            }
            if ($(window).width() < 900) {
                $("header").removeClass("has-open-nav");
            }
        });

        $(window).on("resize", function () {
            if($(window).width() >= 900 && $("header").hasClass("has-open-nav")){
                $("header").removeClass("has-open-nav");
            }
        });

        //Footer FAQ Link
        $("footer a").on("click", function (e) {
            var href = $(this).attr("href");
            if (href.charAt(0) === "#") {
                scrollToAnchor(href);
            }
        });

        //Back to Top
        $("#back-to-top").on("click", function () {
            $('html,body').animate({scrollTop: 0}, 'slow');
        });

        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 10) {
                $("#back-to-top").addClass("show");
            } else {
                $("#back-to-top").removeClass("show");
            }
        })
    },
};
